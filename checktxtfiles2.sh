#!/usr/bin/env bash
###################
#Created by: Tom Lanobel
#Purpose: create a loop that counts the number of files ending in .txt
#version: 0.0.1
###################
set -x
set -u

let count=0

for files in *.txt;

do
if [ $files != "*.txt" ];
then

echo "The number of files is :" $files
let count++

fi	
done

echo "The number of files is :" $count

