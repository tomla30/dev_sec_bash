#!/usr/bin/env bash
set -x
####################
#craeated by: Tom Lanobel
#purpose: examples of operators in bash
#version : 0.0.1
#date: 27.12.2021
###################

#logical operators
#and &&
#or ||
#not !

echo 0 && echo 0 ; echo $?
echo 1 || eho 0 ; echo $?
! echo 1 ; echo $?

#existance operators
#exists -e
#exists and is regular file -f
#exists and is directory -d
#exists and is runnable -x
#exists and is readable -r
#exists and is writeable -w
#does not has any value -z
#has some value -n
#exists and is symbolic link -L

echo '-e' && [[ -e /etc/passwd ]]; echo $?
echo '-f' && [[ -f /etc/passwd ]]; echo $?
echo '-r' && [[ -r /etc/passwd ]]; echo $?
echo '-w' && [[ -w $0 ]]; echo $?
echo '-d' && [[ -d /etc/passwd ]]; echo $?
echo '-x' && [[ -x $0 ]]; echo $?
echo '-L' && [[ -L /etc/passwd ]]; echo $?

echo '-z' && [[ -z $SHELL ]]; echo $?
echo '-n' && [[ -n $SHELL ]]; echo $?
