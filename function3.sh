#!/usr/bin/env bash
##################
#Created by: Tom Lanobel	
#Purpose: learn functions
#version: 0.0.1
#################

function main(){
	deco "$@"
}

function hello_world(){
	line="###############"
	printf "\n$line\n# %s\n$line\n" "Hello world"
}

function deco(){
	
	line="###############"
	printf "\n$line\n# %s\n$line\n" "$@"
}
################## DO not remove #################

main "$@"
