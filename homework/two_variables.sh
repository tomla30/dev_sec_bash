#!/usr/bin/env bash
###################
#Created by: Tom Lnobel
#Purpose: print 2 variables
#Version: 0.0.1
##################
set -e
set -u
set -x

var=Tom_Lanobel
var1=28

#Print the variables on the cli.

echo "My name is $var and I am $var1 years old."

#for question number 5 this is the answer:
#source ./two_variables.sh


#for question number 6 this is the answer:
#. ./two_variables.sh


