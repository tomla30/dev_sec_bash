#!/usr/bin/env bash
###################
#Created by: Tom Lanobel
#Purpose: check if files exists
#version: 0.0.1
###################

set -e
set -u
set -x

if [ -f $1 ]
then echo "file exists"
else echo "file not found"
fi
if [ -f $2 ]
then echo "file exists"
else echo "file not found"
fi
