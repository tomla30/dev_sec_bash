#!/usr/bin/env bash
###################
#Created by: Tom Lanobel
#Purpose: check if files exists
#version: 0.0.1
###################

set -e
set -u
set -x

var=$1
var1=$2
var2=$3
var3=$4

echo "$var3 $var2 $var1 $var"
