#!/usr/bin/env bash
###################
#Created by: Tom Lanobel
#Purpose: check if files exists
#version: 0.0.1
###################

set -e
set -u
set -x

uname2="$(stat --format '%U' "$1")"
if [ -f $1 ] 
then echo "file exists"
if [ "x${uname2}" = "x${USER}" ]; then
	echo "my user is ${USER} owner
if [ -w $1 ] 
then
	echo "write permission is granted on $1"
else
	echo "erite permission is not granted on $1"
chmod u+w $1
echo "erite permissions added"
fi
else 
	echo no owner
fi
else echo $1 not found
fi
