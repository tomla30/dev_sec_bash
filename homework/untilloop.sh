#!/usr/bin/env bash
###################
#Created by: Tom Lanobel
#Purpose: create a until loop that count from 8 to 4
#version: 0.0.1
###################

set -e
set -u
set -x

i=8

until [ $i -lt 4 ]
do
	echo $i;
	let i--
done
