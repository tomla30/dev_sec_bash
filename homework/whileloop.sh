#!/usr/bin/env bash
###################
#Created by: Tom Lanobel
#Purpose: create a while loop that count from 3 to 7
#version: 0.0.1
###################

set -e
set -u
set -x

i=3

while [ $i -le 7 ]
do
	echo $i;
	let i++ 
done
