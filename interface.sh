#!/usr/bin/env bash
#######################
#Created by: Tom Lanobel
#Purpose: print interface and its ip. if interface doesnt have ip, no need to show it
#Version: 0.0.1
#######################

ip -s -br addr show|awk '{print $1 $3}'

