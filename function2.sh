#!/usr/bin/env bash
##################
#Created by: Tom Lanobel	
#Purpose: learn functions
#version: 0.0.1
#################
set -x
function check_root_home(){
	ls -ltRa /root
}

if [[ $UID -eq 0 ]];then
	check_root_home
else
	echo "Please run script with root permissions"
fi
