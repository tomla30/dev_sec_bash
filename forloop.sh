#!/usr/bin/env bash
###################
#Created by: Tom Lanobel
#Purpose: create a for loop that count from 3 to 7
#version: 0.0.1
###################

set -e
set -u
set -x

i=3

for i in 3 4 5 6 7
do
	echo $i
done
