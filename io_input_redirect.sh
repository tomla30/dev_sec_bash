#!/usr/bin/env bash
##################
#Created by: Tom Lanobel	
#Purpose: 
#version: 0.0.1
#################

while read user
do
	if [[ -e /home/$user ]]; then
		echo $user exists in /home
	fi
done < ~/users.system
