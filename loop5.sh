#!/usr/bin/env bash
###################
#created by: Tom Lanobel
#purpose: learning loops
#version: 0.0.1
##################
i=$1
while [[ $i -lt 100 ]]
do
	echo $i
	if ((i%7==0));then
		echo BOOM !!!
		continue
	elif ((i%3==0));then
		echo Gotcha
		break
	fi
	sleep 0.2
	let i++
done
