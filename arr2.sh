#!/usr/bin/env bash
######################
#created by: Tom Lanobel
#Purpose: to work with arrays
#Version: 0.0.1
#####################

name=$1
lname=$2
age=$3

multi=($name $lname $age)

echo "Hello : my name is ${multi[0]} ${multi[1]} and I am ${multi[2]}"
