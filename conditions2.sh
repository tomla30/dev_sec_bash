#!/usr/bib/env bash
###########################
#created by Tom Lnobel
#purpose: learn conditional structs
#version: 0.0.1
#date: 27.12.2021
##########################
set -u
set -e
set -x

var=$1

if [[ $var -ge 42 ]] 
then
        echo "the meaning of life is 42"
elif [[ $var -le 42  ]]
then 
        echo "the meaning of life could be 42...but i am not sure"
fi

