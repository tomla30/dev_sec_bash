#!/usr/bin/env bash

#set -e
set -u
set -x
##########################
#created by: Tom Lanobel
#Purpose: learn conditions
#version: 0.0.1
##########################

num1=5
num2=4
num3=

test $num1 -gt $num2; echo $? # integer greater then
test $num1 -lt $num2; echo $? # integer less then
test $num1 -ge $num2; echo $? # integer greater or equal then
test $num1 -le $num2; echo $? # integer less or equal
test $num1 -ne $num2; echo $? # integer not equal
test $num1 -eq $num2; echo $? # integer equal
