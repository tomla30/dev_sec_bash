#!/usr/bin/env bash
#####################
#Created by: Tom Lanobel
#Purpose: print name of city
#version:0.0.1
#####################
set -e
set -u
set -x

city=Kfar_Saba

echo $city
