#!/usr/bin/env bash
#######################
#Created by: Tom Lanobel
#Purpose: print menu
#Version: 0.0.1
####################

hostname=$"echo hostname -b"
storage=$"echo df -h"


PS3='Please enter your choice: '
options=($"hostame" "storage" "homefolder" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "hostname")
            echo "you chose choice 1"
            ;;
        "storage")
            echo "you chose choice 2"
            ;;
        "Option 3")
            echo "you chose choice $REPLY which is $opt"
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done
