#!/usr?bin?env bash
######################
#created by: Tom Lanobel
#Purpose: to work with positional params
#Version: 0.0.1
#####################

pos_var=$5

echo "the $0 has run and the value of pos_var is $pos_var"
