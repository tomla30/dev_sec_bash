#!/usr/bin/env bash
#######################
#Created by: Tom Lanobel
#Purpose: print menu
#Version: 0.0.1
######################

clear
echo "
please choose from below:
1.Display system storage
2.Display hostname
3.Display $USER' s home space storage
0. Quit
"
read -p "Choose your option [0-3] >" REPLY

case "$REPLY" in

	0) echo "Quiting"; exit 0 ;;
	1) echo "Displaying storage"; df -h ;;
	2) echo "Displaying hostname"; hostnamectl;;
	3)	if [[ $(id -u) -eq 0 ]];then
			du -sh /home/*
		else:
			cd $USER
			du -sh
			sleep 2
			du -h
		fi
		;;
	*) echo "invalid option"; exit 1 ;;
esac



