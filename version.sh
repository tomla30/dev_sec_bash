#!/usr/bin/env bash
#######################
#Created by: Tom Lanobel
#Purpose: 
#Version: 0.0.1
#######################

. /etc/os-release
. /etc/lsb-release
if [[ -z $VERSION_CODENAME ]];then
	echo "Variable is empty"
else
	echo $VERSION_CODENAME
fi

