#!/usr/bin/env bash

#set -e
set -u
set -x
##########################
#created by: Tom Lanobel
#Purpose: learn conditions
#version: 0.0.1
##########################

user=$(cat /etc/passwd| grep tom|awk -F: '{print $3}')
[[ $user -eq 1000 ]]; echo $?
