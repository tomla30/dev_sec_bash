#!/usr/bin/env bash
###################
#Created by: Tom Lanobel
#Purpose: create a for loop that count from 1 to 17000
#version: 0.0.1
###################

set -e
set -u
set -x

i=1

for i in {1..17000}
do
	echo $i
done
