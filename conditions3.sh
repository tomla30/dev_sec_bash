#!/usr/bib/env bash
###########################
#created by Tom Lnobel
#purpose: learn conditional structs
#version: 0.0.1
#date: 27.12.2021
##########################
set -u
set -e
set -x

var=$1

if [[ -n $var ]] 
then
        curl -v $var |grep 'http1/1.1'
else

        echo "the meaning of life is 42"
fi
