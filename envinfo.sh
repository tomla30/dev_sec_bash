#!/usr/bin/env bash

###########################################################################
#My name: Tom Lanobel
#This script save the "shell" value as a variable called "environment" and an "echo" command retrieves the value from the memory and displays it as output.
###########################################################################

environment=$SHELL

echo "${environment}"
