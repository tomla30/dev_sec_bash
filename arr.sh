#!/usr/bin/env bash
set -x
set -e
set -u
######################
#created by: Tom Lanobel
#Purpose: to work with arrays
#Version: 0.0.1
set -e
set -u
#####################

name=$1
lname=$2
age=$3
error=0

multi=($name $lname $age)

echo "Hello : my name is ${multi[0]} ${multi[1]} and I am ${multi[2]}"
