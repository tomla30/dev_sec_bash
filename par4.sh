#!/usr?bin?env bash
######################
#created by: Tom Lanobel
#Purpose: to work with positional params
#Version: 0.0.4
#####################

shift 

name=$1
lname=$2
age=$4

echo "Hello : my name is $name $lname and I am $age"
