#!/usr/bin/env bash
###################
#created by: Tom Lanobel
#purpose: learning loops
#version: 0.0.1
##################

for index in 1 2 3 4 5 6 7
do
	echo $index
done
